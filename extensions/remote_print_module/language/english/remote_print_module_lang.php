<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

$lang['_text_title'] 		                    = 'Remote Print Module';
$lang['text_tab_general'] 	                    = 'General';

$lang['label_title'] 	                        = 'Title';
$lang['label_printer_id'] 	                    = 'Printer ID';
$lang['label_order_url'] 	                    = 'Order Submit URL';
$lang['label_notify_url'] 	                    = 'Order Notification URL';
$lang['label_api_key'] 	                        = 'API Key';
$lang['label_api_password'] 	                = 'API Password';

/* End of file remote_print_module_lang.php */
/* Location: ./extensions/stripe/language/english/remote_print_module_lang.php */