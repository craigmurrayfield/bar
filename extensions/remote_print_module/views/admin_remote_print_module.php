<div class="row content">
	<div class="col-md-12">
		<div class="row wrap-vertical">
			<ul id="nav-tabs" class="nav nav-tabs">
				<li class="active"><a href="#general" data-toggle="tab"><?php echo lang('text_tab_general'); ?></a></li>
			</ul>
		</div>

		<form role="form" id="edit-form" class="form-horizontal" accept-charset="utf-8" method="POST" action="<?php echo current_url(); ?>">
			<div class="tab-content">
				<div id="general" class="tab-pane row wrap-all active">
                    <div class="form-group">
                        <label for="input-title" class="col-sm-3 control-label"><?php echo lang('label_title'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="title" id="input-title" class="form-control" value="<?php echo set_value('title', $title); ?>" />
                            <?php echo form_error('title', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-printer-id" class="col-sm-3 control-label"><?php echo lang('label_printer_id'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="printer_id" id="input-printer-id" class="form-control" value="<?php echo set_value('printer_id', $printer_id); ?>" />
                            <?php echo form_error('printer_id', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-order-url" class="col-sm-3 control-label"><?php echo lang('label_order_url'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="order_url" id="input-order-url" class="form-control" value="<?php echo set_value('order_url', $order_url); ?>" />
                            <?php echo form_error('order_url', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-notify-url" class="col-sm-3 control-label"><?php echo lang('label_notify_url'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="notify_url" id="input-notify-url" class="form-control" value="<?php echo set_value('notify_url', $notify_url); ?>" />
                            <?php echo form_error('notify_url', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-api-key" class="col-sm-3 control-label"><?php echo lang('label_api_key'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="api_key" id="input-api-key" class="form-control" value="<?php echo set_value('api_key', $api_key); ?>" />
                            <?php echo form_error('api_key', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-api-password" class="col-sm-3 control-label"><?php echo lang('label_api_password'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="api_password" id="input-api-password" class="form-control" value="<?php echo set_value('api_password', $api_password); ?>" />
                            <?php echo form_error('api_password', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    </div>
				</div>
			</div>
		</form>
	</div>
</div>