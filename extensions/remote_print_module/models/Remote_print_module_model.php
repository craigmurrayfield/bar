<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Remote_print_module_model extends TI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function sendOrder($order_id) {

		//return $this->sendToPrinter($this->preparePayload($order));
	}

	private function sendToPrinter() {
	}

    private function preparePayload($order) {
        $order_id = '32001';
        $currency = 'USD';

        $order_type = 'delivery';
        $payment_status = 'paid';
        $payment_method = 'creditcard';
        $auth_code = '443AD453454'; //identification code of payment

        $order_time = '18:30 14-08-14'; //h:m dd-mm-yy
        $delivery_time = '18:45 14-08-14'; //h:m dd-mm-yy

        // $deliverycost = 3.50;
        //$card_fee = 2.50;
        $extra_fee = 0.00;
        $total_discount = 1.50;
        $total_amount = 15.00;

        $cust_name = 'Jhon Smith';
        $cust_address = 'Address line1%%address line2%%...';
        $cust_phone = '44034343434';
        $isVarified = 'verified';
        $cust_instruction = 'Make it spicy';
        $num_prev_order = 10;

        //optional data for print settings
        $apply_settings = 0; //0=settings will not be applied, 1=settings will be applied
        $auto_print = 0;
        $auto_accept = 0;
        $enter_delivery_time = 1;
        $time_input_method = 1;
        $time_list = '0-5-10-15-20-25-30-35-40-45-50-55-60';
        $extra_line_feed = 3;

        $menu_item = array(
            array(
                'category'=>'item category 1',
                'name'=>'item name 1',
                'description'=>'item description 1',
                'quantity'=>'1',
                'price'=>'3.50'
            ),
            array(
                'category'=>'item category 2',
                'name'=>'item name 2',
                'description'=>'item description 2',
                'quantity'=>'3',
                'price'=>'6.00'
            )
        );

        //API access credential
        $api_key = 'apikey';
        $api_password = 'secretapipassword';

        //receipt header and footer text
        $receipt_header = "Header line 1/rAddress line 1%%Phone number";
        $receipt_footer = "Thank you";

        //iconnect printer ID
        $printer_id = 2501;

        //notification URL
        $notify_url = '';
        //end data part

        //preparing post fields as array
        $post_array = array();

        $post_array['api_key'] = $api_key;
        $post_array['api_password'] = $api_password;


        $post_array['receipt_header'] = $receipt_header;
        $post_array['receipt_footer'] = $receipt_footer;

        $post_array['notify_url'] = $notify_url;
        $post_array['printer_id'] = $printer_id;

        $post_array['order_id'] = $order_id;
        $post_array['currency'] = $currency;

        //1=Delivery, 2=Collection/Pickup, 3=Reservation
        if($order_type=='delivery'){
            $post_array['order_type'] = 1;
        }
        else if($order_type=='collection' || $order_type=='pickup'){
            $post_array['order_type'] = 2;
        }
        else if($order_type=='reservation'){
            $post_array['order_type'] = 3;
        }

        //6=paid, 7=not paid
        if($payment_status=='paid'){
            $post_array['payment_status'] = 6;
        }
        else{
            $post_array['payment_status'] = 7;
        }
        $post_array['payment_method'] = $payment_method;
        $post_array['auth_code'] = $auth_code;

        $post_array['order_time'] = $order_time;
        $post_array['delivery_time'] = $delivery_time;

        $post_array['deliverycost'] = $deliverycost;
        $post_array['card_fee'] = $card_fee;
        $post_array['extra_fee'] = $extra_fee;
        $post_array['total_discount'] = $total_discount;
        $post_array['total_amount'] = $total_amount;

        $post_array['cust_name'] = $cust_name;
        $post_array['cust_address'] = $cust_address;
        $post_array['cust_phone'] = $cust_phone;

        //4=verified, 5=not verified
        if($isVarified=='verified'){
            $post_array['isVarified'] = 4;
        }
        else{
            $post_array['isVarified'] = 5;
        }
        $post_array['cust_instruction'] = $cust_instruction;
        $post_array['num_prev_order'] = $num_prev_order;

        $post_array['apply_settings'] = $apply_settings;
        $post_array['auto_print'] = $auto_print;
        $post_array['auto_accept'] = $auto_accept;
        $post_array['enter_delivery_time'] = $enter_delivery_time;
        $post_array['time_input_method'] = $time_input_method;
        $post_array['time_list'] = $time_list;
        $post_array['extra_line_feed'] = $extra_line_feed;

        $cnt = 1;
        foreach($menu_item as $val){
            $post_array['cat_'.$cnt] = $val['category'];
            $post_array['item_'.$cnt] = $val['name'];
            $post_array['opt_'.$cnt] = $val['order_option_name']
            $post_array['desc_'.$cnt] = $val['description'];
            $post_array['qnt_'.$cnt] = $val['quantity'];
            $post_array['price_'.$cnt] = $val['price'];

            $cnt++;
        }

//echo '<pre>';
//print_r($post_array);

//post order data to iconnect API
        $iconnect_api_url = 'http://iconnect.ibacstel.com/submitorder.php';
        $response = post_to_api($iconnect_api_url,$post_array);

//print_r($response);
//do your necessary things here based on the response status

        if($response['status']=='OK'){
            //order submitted successfully
        }
        else{
            //order submition failed because of following reason
            //print_r($response['error']);
        }
    }

}

/* End of file Stripe_model.php */
/* Location: ./extensions/stripe/models/Stripe_model.php */