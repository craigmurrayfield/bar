<?php defined('EXTPATH') OR exit('No direct script access allowed');

$config['after_create_order'][] = array(
'module'        => 'remote_print_module',
'filepath'      => 'controllers',
'filename'      => 'Remote_print_module.php',
'class'         => 'Remote_print_module',
'method'        => 'after_create_order'
);