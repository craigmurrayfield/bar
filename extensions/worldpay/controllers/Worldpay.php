<?php if (!defined('BASEPATH')) exit('No direct access allowed');

class Worldpay extends Main_Controller
{

    public function index()
    {
        if (!file_exists(EXTPATH . 'worldpay/views/worldpay.php')) {                    //check if file exists in views folder
            show_404();                                                                        // Whoops, show 404 error page!
        }

        $this->load->model('Worldpay_model');
        $this->lang->load('worldpay/worldpay');

        $payment = $this->extension->getPayment('worldpay');
        $this->template->setScriptTag('https://cdn.worldpay.com/v1/worldpay.js', 'worldpay-js', '200003');
        $this->template->setScriptTag(extension_url('worldpay/views/assets/process-worldpay.js'), 'process-worldpay-js', '200004');

        // START of retrieving lines from language file to pass to view.
        $data['code'] = $payment['name'];
        $data['title'] = !empty($payment['ext_data']['title']) ? $payment['ext_data']['title'] : $payment['title'];
        $data['description'] = !empty($payment['ext_data']['description']) ? $payment['ext_data']['description'] : $this->lang->line('text_description');
        $data['force_ssl'] = isset($payment['ext_data']['force_ssl']) ? $payment['ext_data']['force_ssl'] : '1';
        // END of retrieving lines from language file to send to view.

        $order_data = $this->session->userdata('order_data');                           // retrieve order details from session userdata
        $data['payment'] = !empty($order_data['payment']) ? $order_data['payment'] : '';
        $data['minimum_order_total'] = is_numeric($payment['ext_data']['order_total']) ? $payment['ext_data']['order_total'] : 0;
        $data['order_total'] = $this->cart->total();

        if ($this->input->post('worldpay_token')) {
            $data['worldpay_token'] = $this->input->post('worldpay_token');
        } else {
            $data['worldpay_token'] = '';
        }

        if (isset($this->input->post['worldpay_cardholder'])) {
            $data['worldpay_cardholder'] = $this->input->post['worldpay_cardholder'];
        } else {
            $data['worldpay_cardholder'] = '';
        }

        if (isset($this->input->post['worldpay_cc_number'])) {
            $padsize = (strlen($this->input->post['worldpay_cc_number']) < 7 ? 0 : strlen($this->input->post['worldpay_cc_number']) - 7);
            $data['worldpay_cc_number'] = substr($this->input->post['worldpay_cc_number'], 0, 4) . str_repeat('X', $padsize) . substr($this->input->post['worldpay_cc_number'], -3);
        } else {
            $data['worldpay_cc_number'] = '';
        }

        if (isset($this->input->post['worldpay_cc_exp_month'])) {
            $data['worldpay_cc_exp_month'] = $this->input->post('worldpay_cc_exp_month');
        } else {
            $data['worldpay_cc_exp_month'] = '';
        }

        if (isset($this->input->post['worldpay_cc_exp_year'])) {
            $data['worldpay_cc_exp_year'] = $this->input->post('worldpay_cc_exp_year');
        } else {
            $data['worldpay_cc_exp_year'] = '';
        }

        if (isset($this->input->post['worldpay_cc_cvc'])) {
            $data['worldpay_cc_cvc'] = $this->input->post('worldpay_cc_cvc');
        } else {
            $data['worldpay_cc_cvc'] = '';
        }

        // pass array $data and load view files
        return $this->load->view('worldpay/worldpay', $data, TRUE);
    }

    public function confirm()
    {
        $this->lang->load('worldpay/worldpay');

        $this->form_validation->reset_validation();
        $this->form_validation->set_rules('worldpay_token', 'lang:label_card_number', 'xss_clean|trim|required');

        if ($this->form_validation->run() === TRUE) {                                            // checks if form validation routines ran successfully
            $validated = TRUE;
        } else {
            return FALSE;
        }

        $order_data = $this->session->userdata('order_data');                        // retrieve order details from session userdata
        $cart_contents = $this->session->userdata('cart_contents');                                                // retrieve cart contents

        if ($validated === TRUE AND !empty($order_data['payment']) AND $order_data['payment'] == 'worldpay') {    // check if payment method is equal to worldpay

            if (empty($order_data) OR empty($cart_contents)) {
                return FALSE;
            }

            $ext_payment_data = !empty($order_data['ext_payment']['ext_data']) ? $order_data['ext_payment']['ext_data'] : array();

            if (!empty($ext_payment_data['order_total']) AND $cart_contents['order_total'] < $ext_payment_data['order_total']) {
                $this->alert->set('danger', $this->lang->line('alert_min_total'));
                return FALSE;
            }

            //Pass the cardholders name with the order data
            $order_data['cardholder_name'] = $this->input->post('name') != null ? $this->input->post('name') : '';

            $this->load->model('Worldpay_model');
            $response = $this->Worldpay_model->createCharge($this->input->post('worldpay_token'), $order_data);

            log_message('error', json_encode($response));

            if ($response['paymentStatus'] !== 'SUCCESS') {
                $this->alert->set('danger', 'Something went wrong while charging the supplied card. Please double check your details or contact the seller directly.');
            } else {
                if ($response['paymentStatus'] !== 'SUCCESS') {
                    $order_data['status_id'] = $ext_payment_data['order_status'];
                } else if (isset($ext_payment_data['order_status']) AND is_numeric($ext_payment_data['order_status'])) {
                    $order_data['status_id'] = $ext_payment_data['order_status'];
                } else {
                    $order_data['status_id'] = $this->config->item('default_order_status');
                }

                if (!empty($response['paymentStatus'])) {
                    $comment = sprintf($this->lang->line('text_payment_status'), $response['paymentStatus'], $response['customerOrderCode']);
                } else {
                    $comment = "Payment failed for order {$response['customerOrderCode']}";
                }

                $order_history = array(
                    'object_id' => $order_data['order_id'],
                    'status_id' => $order_data['status_id'],
                    'notify' => '0',
                    'comment' => $comment,
                    'date_added' => mdate('%Y-%m-%d %H:%i:%s', time()),
                );

                log_message('error', json_encode($order_data));

                $this->load->model('Statuses_model');
                $this->Statuses_model->addStatusHistory('order', $order_history);

                $this->load->model('Orders_model');
                if ($this->Orders_model->completeOrder($order_data['order_id'], $order_data, $cart_contents)) {
                    redirect('checkout/success');                                    // redirect to checkout success page with returned order id
                }
            }

            return FALSE;
        }
    }
}

/* End of file worldpay.php */
/* Location: ./extensions/worldpay/controllers/worldpay.php */