<?php if (!defined('BASEPATH')) exit('No direct access allowed');

class Worldpay_model extends TI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('cart');
        $this->load->library('currency');
    }

    public function createCharge($token, $order_data = array())
    {
        if (empty($token) OR empty($order_data['order_id'])) {
            return FALSE;
        }

        $currency = $this->currency->getCurrencyCode();
        $order_total = $this->cart->order_total() * 100;//round((float)$this->cart->order_total(), 2);
        //$handling_fee = round($order_total / 100 * 5, 2) * 100 + 100;

        $payment = $this->extension->getPayment('worldpay');
        $settings = !empty($payment['ext_data']) ? $payment['ext_data'] : array();

        $secretKey = '';
        if (isset($settings['live_publishable_key']) AND $settings['transaction_mode'] === 'live') {
            $secretKey = $settings['live_publishable_key'];
        } else if (isset($settings['test_publishable_key'])) {
            $secretKey = $settings['test_publishable_key'];
        }

        require_once BASEPATH . '../vendor/worldpay/worldpay-lib-php/init.php';
        $worldpay = new Worldpay\Worldpay($secretKey);

        // Create a WorldPay order and retrieve the response
        try {
            $response = $worldpay->createOrder(array(
                'token' => $token,
                'amount' => $order_total, // + $handling_fee,
                'currencyCode' => $currency,
                'name' => $order_data['cardholder_name'],
                'billingAddress' => null, // billing address is not a required field but it is recommended
                'orderDescription' => sprintf($this->lang->line('text_worldpay_charge_description'), $this->config->item('site_name'), $order_data['email']),
                'customerOrderCode' => $order_data['order_id']
            ));
            if ($response['paymentStatus'] === 'SUCCESS') {
                $worldpayOrderCode = $response['orderCode'];
                return $response;
            } else {
                throw new \Worldpay\WorldpayException(print_r($response, true));
            }
        } catch (\Worldpay\WorldpayException $e) {
            log_message('error', $e);
        } catch (Exception $e) {
            log_message('error', $e);
        }
        return null; // something went wrong
    }
}

/* End of file Worldpay_model.phphp */
/* Location: ./extensions/stripe/models/Worldpay_model.phphp */