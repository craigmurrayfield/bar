jQuery(function () {
    var form = document.getElementById('checkout-form');
    var $button = $('#cart-box .cart-buttons .btn');

    $('#checkout-form').submit(function (event) {
        var $form = $(this);
        var $paymentInput = $form.find('input[name="payment"]:checked');

        if ($paymentInput.val() === 'worldpay') {
            Worldpay.useOwnForm({
                'clientKey': 'T_C_8dbaef82-d1be-4188-a4d7-15ee93600b95',
                'form': form,
                'reusable': false,
                'callback': function (status, response) {
                    document.getElementById('paymentErrors').innerHTML = '';
                    if (response.error) {
                        appendError(response.error.message);
                    } else {
                        $button.addClass('disabled');
                        var token = response.token;
                        $('#worldpay_token').val(token);
                        $form.get(0).submit();
                    }
                }
            });
            // Prevent the form from submitting with the default action
            event.preventDefault();
            return false;
        }
    });

    function appendError(message) {
        if (message.length > 0)
            $('#checkout-form .worldpay-errors').html('<p class="alert alert-danger">' + message + '</p>');
        $button.removeClass('disabled');
    }
});