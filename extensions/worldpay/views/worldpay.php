<div class="radio">
    <label>
        <?php if ($minimum_order_total >= $order_total) { ?>
            <input type="radio" name="payment" value="" <?php echo set_radio('payment', ''); ?> disabled/>
        <?php } else if ($payment === $code) { ?>
            <input type="radio" name="payment"
                   value="<?php echo $code; ?>" <?php echo set_radio('payment', $code, TRUE); ?> />
        <?php } else { ?>
            <input type="radio" name="payment"
                   value="<?php echo $code; ?>" <?php echo set_radio('payment', $code); ?> />
        <?php } ?>
        <?php echo $title; ?> - <span><?php echo $description; ?></span>
    </label>
    <?php if ($minimum_order_total >= $order_total) { ?>
        <br/><span
                class="text-info"><?php echo sprintf(lang('alert_min_order_total'), currency_format($minimum_order_total)); ?></span>
    <?php } ?>
</div>
<div id="worldpay-payment" class="wrap-horizontal"
     style="<?php echo ($payment === 'authorize_net_aim') ? 'display: block;' : 'display: none;'; ?>">
    <input type="hidden" id="worldpay_token" name="worldpay_token"/>

    <span id="paymentErrors"></span>

    <div class="row">
        <div class="col-xs-12">
            <div class="worldpay-errors"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div style="font-size: 0.8em;padding-bottom: 21px;" id="secure-payment">
                <img src="<?php echo assets_url('images/data/cc/locked.png') ?>"
                     alt="lock secure icon" draggable="false">&nbsp;YOUR CREDIT / DEBIT CARD WILL BE SECURELY PROCESSED
            </div>
            <div class="form-group">
                <br/>
                <label for="name"><?php echo lang('label_card_cardholder'); ?></label>
                <div class="input-group" style="width: 100%;">
                    <input type="text" name="name" data-worldpay="name" class="form-control"
                           value="<?php echo set_value('worldpay_cardholder', $worldpay_cardholder); ?>"
                           placeholder="<?php echo lang('text_cardholder'); ?>" required/>
                </div>
                <br/>
                <label for="input-card-number"><?php echo lang('label_card_number'); ?></label>
                <div class="input-group">
                    <input type="tel" id="input-card-number" class="form-control" size="20" data-worldpay="number"
                           value="<?php echo set_value('worldpay_cc_number', $worldpay_cc_number); ?>"
                           placeholder="<?php echo lang('text_cc_number'); ?>" autocomplete="cc-number" required data-numeric/>
                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                </div>
                <br/>
                <img src="<?php echo assets_url('images/data/cc/Visa.png') ?>" alt="Visa" style="height: 40px">
                <img src="<?php echo assets_url('images/data/cc/VisaElectron.png') ?>" alt="Visa Electron"
                     style="height: 40px">
                <img src="<?php echo assets_url('images/data/cc/MasterCard.png') ?>" alt="Master Card"
                     style="height: 40px">
                <img src="<?php echo assets_url('images/data/cc/Maestro.png') ?>" alt="Maestro"
                     style="height: 40px">
                <?php echo form_error('worldpay_cc_number', '<span class="text-danger">', '</span>'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-7 col-md-7">
            <div class="form-group">
                <label for="input-expiry-month"><?php echo lang('label_card_expiry'); ?></label>
                <div class="row">
                    <div class="col-xs-6 col-lg-6">
                        <input type="tel" class="form-control" id="input-expiry-month"
                               value="<?php echo set_value('worldpay_cc_exp_month', $worldpay_cc_exp_month); ?>"
                               placeholder="<?php echo lang('text_exp_month'); ?>" autocomplete="off" size="2"
                               data-worldpay="exp-month" required data-numeric/>
                    </div>
                    <div class="col-xs-6 col-lg-6">
                        <input type="tel" class="form-control" id="input-expiry-year"
                               value="<?php echo set_value('worldpay_cc_exp_year', $worldpay_cc_exp_year); ?>"
                               placeholder="<?php echo lang('text_exp_year'); ?>" autocomplete="off" size="4"
                               data-worldpay="exp-year" required data-numeric/>
                    </div>
                </div>
                <?php echo form_error('worldpay_cc_exp_month', '<span class="text-danger">', '</span>'); ?>
                <?php echo form_error('worldpay_cc_exp_year', '<span class="text-danger">', '</span>'); ?>
            </div>
        </div>
        <div class="col-xs-5 col-md-5 pull-right">
            <div class="form-group">
                <label for="input-card-cvc"><?php echo lang('label_card_cvc'); ?></label>
                <input type="tel" class="form-control" autocomplete="off" size="4" data-worldpay="cvc"
                       value="<?php echo set_value('worldpay_cc_cvc', $worldpay_cc_cvc); ?>"
                       placeholder="<?php echo lang('text_cc_cvc'); ?>"/>
                <?php echo form_error('worldpay_cc_cvc', '<span class="text-danger">', '</span>'); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    var forceSSL = "<?php echo $force_ssl; ?>";
    $(document).ready(function () {
        $('input[name="payment"]').on('change', function () {
            if (this.value === 'worldpay') {
                if (forceSSL == '1' && location.href.indexOf("https://") == -1) {
                    location.href = location.href.replace("http://", "https://");
                }

                $('#worldpay-payment').slideDown();
            } else {
                $('#worldpay-payment').slideUp();
            }
        });

        $('input[name="payment"]:checked').trigger('change');
    });
    --></script>
