<?php defined('EXTPATH') OR exit('No direct script access allowed');

$config['after_create_order_to_hub'][] = array(
'module'        => 'remote_hub_module',
'filepath'      => 'controllers',
'filename'      => 'Remote_hub_module.php',
'class'         => 'Remote_hub_module',
'method'        => 'after_create_order_to_hub'
);