</div>
</div>
<div class="loader"></div>

<!--<div id="facebook-page-module">-->
<!--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FDalgetyFishBar%2F&tabs&width=340&height=130&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="250" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->
<!--</div>-->
<footer id="page-footer">
    <?php echo get_partial('content_footer'); ?>
    <div id="footer-container" class="container">
        <div class="row" id="footer-button">
            <div class="col-xs-12">
                <ul id="footer-buttons" class="nav nav-pills nav-justified thumbnail">
                    <li class="nav-button">
                        <a class="list-group-item-heading footer-item<?php echo page_url() == site_url('local') ? ' active' : '' ?>"
                           href="<?php echo site_url('local?location_id=11'); ?>"><i
                                    class="fa fa-cutlery fa-lg"></i> <h4>Menu</h4><i></i></a>
                    </li>
                    <li class="nav-button">
                        <a class="list-group-item-heading footer-item<?php echo page_url() == site_url('account/orders') ? ' active' : '' ?>"
                           href="<?php echo site_url('account/orders'); ?>"> <i
                                    class="fa fa-shopping-cart fa-lg"></i> <h4>Orders</h4></a>
                    </li>
                    <li class="nav-button">
                        <a class="list-group-item-heading footer-item<?php echo page_url() == site_url('about-us') ? ' active' : '' ?>"
                           href="<?php echo site_url('about-us'); ?>"><i
                                    class="fa fa-thumbs-o-up fa-lg"></i> <h4>About us</h4></a>
                    </li>
                    <li class="nav-button account-item">
                        <a class="list-group-item-heading footer-item<?php $current_url = explode('/', trim(str_replace(site_url(), '', page_url()), '/'));
                        echo $current_url[0] == 'account' && page_url() !== site_url('account/orders') ? ' active' : ''; ?>"
                           href="#"><i
                                    class="fa fa-user-plus fa-lg"></i> <h4>Account</h4></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--<div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 wrap-all border-top">
                </div>
            </div>
        </div>
    </div>-->

    <script type="text/javascript">
        $('.account-item').on('click', function () {
            $('nav#sidebar').toggleClass('active');
        });

        $('span.navbar-trigger-close > a').on('click', function () {
            $('nav#sidebar').toggleClass('active');
        });

        $('#account-options-href').on('click', function () {
            $('#account-options-chevron').toggleClass('fa-chevron-up fa-chevron-down');
        });

        $(window).load(function () {
            $(".loader").fadeOut("slow");
            setTimeout(function () {
                $("#page-footer").hide().fadeIn('fast');
            }, 100);
        });

        // hide the footer when input is active
        $("input").blur(function () {
            $("#page-footer").fadeIn('fast');
        });

        $("input").focus(function () {
            $("#page-footer").hide();
        });
    </script>
</footer>
<?php $custom_script = get_theme_options('custom_script'); ?>
<?php if (!empty($custom_script['footer'])) {
    echo '<script type="text/javascript">' . $custom_script['footer'] . '</script>';
}; ?>
</body>
</html>