<?php echo get_header(); ?>
<div id="logo-header">
    <div class="container">
        <div class="row">
            <div class="logo">
                <a class="" href="<?php echo rtrim(site_url(), '/') . '/'; ?>">
                    <?php if (get_theme_options('logo_image')) { ?>
                        <img alt="<?php echo $this->config->item('site_name'); ?>"
                             src="<?php echo image_url(get_theme_options('logo_image')) ?>" class="img-responsive">
                    <?php } else if (get_theme_options('logo_text')) { ?>
                        <?php echo get_theme_options('logo_text'); ?>
                    <?php } else if ($this->config->item('site_logo') === 'data/no_photo.png') { ?>
                        <?php echo $this->config->item('site_name'); ?>
                    <?php } else { ?>
                        <img alt="<?php echo $this->config->item('site_name'); ?>"
                             src="<?php echo image_url($this->config->item('site_logo')) ?>" class="img-responsive">
                    <?php } ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php echo get_partial('content_top'); ?>

<?php if ($this->alert->get()) { ?>
    <div id="notification">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->alert->display(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php echo get_footer(); ?>

</div>
</div>
<style>
    body {
        height: 100%;
    }

    /*body {
        background-color: #ffffff;
        background: url(<?php echo substr(site_url(), 0, -10) . '/assets/images/data/s-9.png'; ?>) no-repeat center center fixed;
        -webkit-background-size: auto 100%;
        -moz-background-size: auto 100%;
        -o-background-size: auto 100%;
        background-size: auto 100%;
        background-size: cover;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
    }*/

    .nav > li > a:hover {
        text-decoration: none;
        background-color: #FAFAFA;
    }

    #content-top {
        padding-bottom: 0;
    }

    #main-header {
        display: none;
    }

    #logo-header .logo {
        text-align: center;
    }

    #local-box .panel-local {
        background-color: #EF473D;
    }

    #local-box .panel {
        border-color: #EF473D;
    }

</style>
</body>
<script type="text/javascript">
    window.addEventListener('orientationchange', function () {
        scrollToTop();
    });

    $(function () {
        scrollToTop();
    });

    var scrollToTop = function () {
        $('html, body').animate({
            scrollTop: $("#local-search").offset().top
        }, 500);
    };
</script>
</html>
