<?php if (!defined('BASEPATH')) exit('No direct access allowed');

class Order extends Main_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('location');
        $this->location->initialize();

        $this->load->model('Categories_model');
        $this->load->model('Menu_options_model');
        $this->load->model('Menus_model');
        $this->load->model('Orders_model');

        $this->load->library('currency');
    }

    public function testStatistics()
    {
//        $this->response->addHeader('Content-Type: application/json');
//        $data = $this->input->get('product');

        $use_location = $this->config->item('default_location_id');

        $this->location->setLocation($use_location);

//        $data['local_info'] = $this->location->local();                                        // retrieve local location data
        $data['location_id'] = $this->location->getId();                                        // retrieve local location data
        $data['location_name'] = $this->location->getName();                                        // retrieve local location data
//        $data['location_address'] = $this->location->getAddress();                                        // retrieve local location data
        $data['location_image'] = $this->location->getImage();

//        $data = $this->input->get('product');
        $this->output->set_output(json_encode($data));
    }

    public function getLocation()
    {

        $use_location = $this->config->item('default_location_id');

        $this->location->setLocation($use_location);

//        $data['local_info'] = $this->location->local();                                        // retrieve local location data
        $data['location_id'] = $this->location->getId();                                        // retrieve local location data
        $data['location_name'] = $this->location->getName();                                        // retrieve local location data
//        $data['location_address'] = $this->location->getAddress();                                        // retrieve local location data
        $data['location_image'] = $this->location->getImage();

        $this->output->set_output(json_encode($data));
    }

    public function add()
    {
        $data = $this->input->post('data');
//        $this->output->set_output(json_encode($data));

        $order_info = array();
        //demo data
        $order_info['order_date'] = '2017-08-04';
        $order_info['order_time'] = '2017-08-04 11:57:49';
        $order_info['location_id'] = 11;
        $order_info['order_type'] = '3';
        $order_info['payment'] = 'In Store Payment';

        $cart_contents = array();

//        $cart_contents['order_total'] = '12.2000';
//        $cart_contents['total_items'] = 2;//

        $test_string = '
        {"cart_total":26.1,"total_items":5,"order_total":27.1,"totals":{"delivery":null},"9bf31c7ff062936a96d3c8bd1f8f2ff3":{"rowid":"9bf31c7ff062936a96d3c8bd1f8f2ff3","id":"15","name":"Jumbo Sausage (2)","qty":2,"price":4.3,"comment":"","options":[],"subtotal":8.6},"aab3238922bcc25a6f606eb525ffdc56":{"rowid":"aab3238922bcc25a6f606eb525ffdc56","id":"14","name":"Fish Supper","qty":1,"price":6.9,"comment":"","options":[],"subtotal":6.9},"36923c10805c2fe495c9c462352973d7":{"rowid":"36923c10805c2fe495c9c462352973d7","id":"13","name":"Cheese & Tomato Pizza","qty":2,"price":5.3,"comment":"","options":{"69":[{"value_id":"146","value_name":"BBQ Base","value_price":"0.0000"}]},"subtotal":10.6}}
        ';

        $cart_contents = json_decode($test_string, true);

//        $order_info = $this->Orders_model->getOrder(20221);

        $order_data['order_id'] = $this->Orders_model->addOrder($order_info, $cart_contents);

//        Events::trigger('after_create_order_to_hub', array('order_id' => $order_id));

        $this->output->set_output(json_encode($order_data));
    }

    public function updateOrderStatus()
    {
        $order_id = $this->input->post('order_id');
        $status_id = $this->input->post('status_id');

        if (intval($order_id) && intval($status_id)) {

            if($this->Orders_model->getOrder($order_id) == $order_id){
                $this->output->set_output(json_encode(array(
                    'error' => 'cant find order'
                )));
            }else{
                $this->db->set('status_id', $status_id);
                $this->db->set('date_modified', mdate('%Y-%m-%d', time()));
                $this->db->where('order_id', $order_id);

                if ($this->db->update('orders')) {
                    $this->output->set_output(json_encode(array(
                        'info' => 'success'
                    )));
                }else{
                    $this->output->set_output(json_encode(array(
                        'error' => 'cant update order'
                    )));
                }
            }

        } else {
            $this->output->set_output(json_encode(array(
                'error' => 'invalid input'
            )));
        }
    }

}