<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

/**
 * Created by PhpStorm.
 * User: raulismasiukas
 * Date: 11/07/2017
 * Time: 10:00
 */

class Reviews extends Main_Controller {

    public function __construct() {
        parent::__construct(); 																	// calls the constructor

        $this->load->model('Reviews_model');
        $this->lang->load('local');
    }

    public function index() {
        $this->lang->load('reviews');

        $this->template->setTitle($this->lang->line('text_heading'));

        $data = array();

        $data['local_reviews'] = $this->reviews();

        $this->template->render('reviews', $data);
    }

    public function reviews($data = array()) {
        $date_format = ($this->config->item('date_format')) ? $this->config->item('date_format') : '%d %M %y';

        $url = '&';
        $filter = array();
        $filter['location_id'] = (int) $this->location->getId();

        if ($this->input->get('page')) {
            $filter['page'] = (int) $this->input->get('page');
        } else {
            $filter['page'] = '';
        }

        if ($this->config->item('page_limit')) {
            $filter['limit'] = $this->config->item('page_limit');
        }

        $filter['filter_status'] = '1';

        $ratings = $this->config->item('ratings');
        $data['ratings'] = $ratings['ratings'];

        $data['reviews'] = array();
        $results = $this->Reviews_model->getList($filter);                                    // retrieve all customer reviews from getMainList method in Reviews model
        foreach ($results as $result) {
            $data['reviews'][] = array(                                                            // create array of customer reviews to pass to view
                'author'   => $result['author'],
                'city'     => $result['location_city'],
                'quality'  => $result['quality'],
                'delivery' => $result['delivery'],
                'service'  => $result['service'],
                'date'     => mdate($date_format, strtotime($result['date_added'])),
                'text'     => $result['review_text']
            );
        }

        $prefs['base_url'] = site_url('local?location_id='.$this->location->getId() . $url);
        $prefs['total_rows'] = $this->Reviews_model->getCount($filter);
        $prefs['per_page'] = $filter['limit'];

        $this->load->library('pagination');
        $this->pagination->initialize($prefs);

        $data['pagination'] = array(
            'info'  => $this->pagination->create_infos(),
            'links' => $this->pagination->create_links()
        );

        return $data;
    }
}


/* End of file reviews.php */
/* Location: ./main/controllers/reviews.php */